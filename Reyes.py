######################################################
#########  Solución Parcial Segundo Corte  ###########
######## Electiva Técnica de Profundización I ########
######################################################

class alarma:

  def __init__(self, SenE1, SenM1, SenE2, SenM2): 
    self.SenE1=SenE1   #atributos cargados desde objeto
    self.SenM1=SenM1
    self.SenE2=SenE2
    self.SenM2=SenM2
    self.parlante=0
    self.falla= 0
    
  def estado(self):    #método para verificar entradas
    if self.error() == 0:  #llamada método de validación
      
      # verificación de casos sensor puerta y movimiento activos
      if (self.SenE1 == 5  and self.SenM1 == 5) or (self.SenE2 ==5  and self.SenM2 == 5) :
        self.parlante=1
        self.falla=0
      
      # verificación de casos sensor puerta activo y movimiento inactivo
      elif (self.SenE1 ==5  and self.SenM1 == 0) or (self.SenE2 ==5  and self.SenM2 == 0) :
        self.parlante=1
        self.falla=1
    
    else:    #caso para falla en el sistema de alarma
      print("Segundo cambio para Github")
      self.parlante = 2
      
      
  #método para validar el sistema de alarma - caso prohibido

  def error(self): 
    if self.SenE1 ==5 and self.SenM2 == 5:
      self.falla = 1
    else:
      self.falla = 0
    print(self.falla)
    return self.falla


Intruso1 = alarma(0,0,5,0)   #instancia de la clase
Intruso1.estado()    # llamado a método estado desde objeto
print(f"Intruso en la casa si parlante es 1, falla si es 2, el parlante esta en {Intruso1.parlante}")
print(f"Falla en el sistema si sensor es 1, el sensor esta en {Intruso1.falla}")


######################################################################





  



  






